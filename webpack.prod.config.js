require('dotenv').config();
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const clientDir = path.join(__dirname, 'src');
const publicDir = path.join(__dirname, 'public');
const assetsDir = path.resolve(__dirname, 'prod-build/dist');
const nodeModulesDir = path.resolve(__dirname, 'node_modules');
const mainIndex = path.resolve(__dirname, 'src/index.js');
const templateIndex = path.resolve(__dirname, 'public/index.html');
const favicon = path.resolve(__dirname, 'public/favicon.ico');

const config = {
  devtool: 'source-map',
  mode: 'production',
  entry: {
    app: [
      '@babel/polyfill',
      mainIndex
    ],
    vendor: [
      'react',
      'react-dom',
      'history',
      'jquery',
      'prop-types',
      'react-helmet',
      'react-redux',
      'react-router',
      'react-router-dom',
      'react-router-redux',
      'connected-react-router',
      'redux',
      'redux-logger',
      'redux-saga',
      'reduxsauce',
      'seamless-immutable'
    ]
  },
  output: {
    path: assetsDir,
    publicPath: './',
    filename: '[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js|.jsx?$/,
        exclude: [nodeModulesDir],
        include: clientDir,
        options: {
          babelrc: false,
          presets: [
            "@babel/preset-env", 
            "@babel/preset-react"
          ],
          plugins: [
            "@babel/plugin-transform-runtime",
            "@babel/proposal-class-properties",
            "@babel/plugin-transform-react-constant-elements",
            "transform-react-remove-prop-types",
          ]
        },
        loader: 'babel-loader',
      },
      {
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
              publicPath: url => `./fonts/${url}`,
            }
          }
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100,
              mimetype: 'application/font-woff',
              name: '[name].[ext]',
              outputPath: 'fonts/',
              publicPath: url => `./fonts/${url}`,
            }
          }
        ]
      },
      {
        test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100,
              mimetype: 'application/octet-stream',
              name: '[name].[ext]',
              outputPath: 'fonts/',
              publicPath: url => `./fonts/${url}`,
            }
          }
        ]
      },
      {
        test: /\.svg?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100,
              mimetype: 'image/svg+xml',
              name: '[name].[ext]',
              outputPath: 'images/',
              publicPath: url => `./images/${url}`,
            }
          }
        ]
      },
      {
        test: /\.(png|PNG|jpg|JPG|jpe?g|JPE?G|gif)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100,
              name: '[name].[ext]',
              outputPath: 'images/',
              publicPath: url => `./images/${url}`,
            },
          },
        ],
      },
      {
        test: /(\.css|\.scss|\.sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }, {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('cssnano'),
                require('autoprefixer'),
              ],
              sourceMap: true
            }
          }, {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: [
                  path.resolve(__dirname, 'src/assets'), 
                  path.resolve(__dirname, 'src/components'), 
                  path.resolve(__dirname, 'src/containers'), 
                  path.resolve(__dirname, 'src')
                ],
              },
              sourceMap: true
            }
          }
        ]
      },
    ],
  },
  plugins: [
    setNodeEnv(),
    new HtmlWebpackPlugin({
      template: templateIndex,
      filename: 'index.html',
      inject: 'body',
      favicon: favicon,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new CopyWebpackPlugin(
      [{
          context: './',
          from: '**/*.php',
          to: './',
          force: true
      }], {
          copyUnmodified: true
      }
    ),
    getImplicitGlobals(),
    new MiniCssExtractPlugin({
      filename: '[name][contenthash].css',
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new CompressionPlugin({
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
    new webpack.NamedModulesPlugin(),
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new OptimizeCSSAssetsPlugin({}),
      new TerserPlugin({}),
    ],
    namedModules: false,
    namedChunks: false,
    nodeEnv: process.env.NODE_ENV,
    flagIncludedChunks: true,
    occurrenceOrder: true,
    sideEffects: true,
    usedExports: true,
    concatenateModules: true,
    noEmitOnErrors: true,
    removeAvailableModules: true,
    removeEmptyChunks: true,
    mergeDuplicateChunks: true,
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          filename: 'app.vendor.[contenthash].bundle.js',
          enforce: true,
        },
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '~',
      name: true,
    },
    runtimeChunk: true,
  },
  performance: {
    hints: false,
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.json'],
    alias: {
      Assets: path.resolve(__dirname, "src/assets"),
      Components: path.resolve(__dirname, "src/components"),
      Containers: path.resolve(__dirname, "src/containers"),
      App: path.resolve(__dirname, "src/app"),
      Routes: path.resolve(__dirname, "src/routes"),
      Utils: path.resolve(__dirname, "src/utils")
    },
    modules: ['node_modules', 'app'],
    mainFields: ['browser', 'jsnext:main', 'main']
  },
  target: 'web', // Make web variables accessible to webpack, e.g. window
};

function getImplicitGlobals() {
  return new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery',
    PUBLIC_URL: publicDir,
  });
}

function setNodeEnv() {
  return new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      PUBLIC_URL: JSON.stringify(process.env.PUBLIC_URL),
      NODE_PATH: JSON.stringify(process.env.NODE_PATH)
    },
  });
}

module.exports = config;