# Drag and Drop Components React Web

## Getting Started

### Installation

```
git clone https://gitlab.com/ravipinx/drag-and-drop-components-react-web.git
cd drag-and-drop-components-react-web
npm install
```

### Development

Hot reloading via webpack middlewares:

```$ npm start```

Build for Development:

```$ npm run build-dev```

Build for Production:

```$ npm run build```

## How do I deploy this?

`npm run build`. This will prepare and build the project for production use.

- Places the resulting built project files into `prod-build/dist` directory. (This is the folder you'll put on server).