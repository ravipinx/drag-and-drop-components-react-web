require('dotenv').config();
const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const clientDir = path.join(__dirname, 'src');
const publicDir = path.join(__dirname, 'public');
const assetsDir = path.resolve(__dirname, 'dev-build/dist');
const nodeModulesDir = path.resolve(__dirname, 'node_modules');
const mainIndex = path.resolve(__dirname, 'src/index.js');
const templateIndex = path.resolve(__dirname, 'public/index.html');
const favicon = path.resolve(__dirname, 'public/favicon.ico');

const config = {
  devtool: '#source-map',
  mode: 'development',
  entry: {
    app: [
      require.resolve('react-app-polyfill/ie11'),
      'webpack-hot-middleware/client?reload=true',
      mainIndex
    ],
    vendor: [
      'react',
      'react-dom',
      'history',
      'jquery',
      'prop-types',
      'react-helmet',
      'react-redux',
      'react-router',
      'react-router-dom',
      'react-router-redux',
      'connected-react-router',
      'redux',
      'redux-logger',
      'redux-saga',
      'reduxsauce',
      'seamless-immutable'
    ],
  },
  output: {
    path: assetsDir,
    publicPath: '/dist/',
    filename: '[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js|.jsx?$/,
        exclude: [nodeModulesDir],
        include: clientDir,
        loader: 'babel-loader',
        query  :{
          presets:["@babel/preset-env","@babel/preset-react"]
        }
      },
      // {
      //   'test': /\.css$/,
      //   'use': [
      //     MiniCssExtractPlugin.loader,
      //     'css-loader',
      //   ],
      // },
      // {
      //   'test': /\.scss$/,
      //   'use': [
      //     // MiniCssExtractPlugin.loader,
      //     'css-loader',
      //     'sass-loader',
      //   ],
      // },
      {
        test: /(\.css|\.scss|\.sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            }
          }, 
          {
            loader: 'sass-loader',
            options: {
              implementation: require('node-sass'),
              sassOptions: {
                includePaths: [path.resolve(__dirname, 'src/assets'), path.resolve(__dirname, 'src/components'), path.resolve(__dirname, 'src/containers'), path.resolve(__dirname, 'src')],
              },
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
              // publicPath: url => `./fonts/${url}`,
            }
          }
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              mimetype: 'application/font-woff',
              name: '[name].[ext]',
              outputPath: 'fonts/',
              // publicPath: url => `./fonts/${url}`,
            }
          }
        ]
      },
      {
        test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              mimetype: 'application/octet-stream',
              name: '[name].[ext]',
              outputPath: 'fonts/',
              // publicPath: url => `./fonts/${url}`,
            }
          }
        ]
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100,
              mimetype: 'image/svg+xml',
              name: '[name].[ext]',
              outputPath: 'images/',
              // publicPath: url => `./images/${url}`,
            }
          }
        ]
      },
      {
        test: /\.(png|PNG|jpe?g|JPE?G|gif)(\?\S*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100,
              name: '[name].[ext]',
              outputPath: 'images/',
              // publicPath: url => `./images/${url}`,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(), // Tell webpack we want hot reloading
    new HtmlWebpackPlugin({
      template: templateIndex,
      filename: 'index.html',
      inject: 'body',
      favicon: favicon,
    }),
    getImplicitGlobals(),
    setNodeEnv(),
    new MiniCssExtractPlugin({
      filename: '[name][hash].css',
    }),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          filename: 'app.vendor.[hash].bundle.js',
          enforce: true,
        },
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
    runtimeChunk: false,
  },
  resolve: {
    modules: ['node_modules', 'app'],
    extensions: ['.js', '.jsx', '.json', '.react.js'],
    mainFields: ['browser', 'jsnext:main', 'main'],
    alias: {
      Assets: path.resolve(__dirname, "src/assets"),
      Components: path.resolve(__dirname, "src/components"),
      Containers: path.resolve(__dirname, "src/containers"),
      App: path.resolve(__dirname, "src"),
      Routes: path.resolve(__dirname, "src/routes"),
      Utils: path.resolve(__dirname, "src/utils")
    }
  },
  devServer: {
    historyApiFallback: true
  },
  target: 'web', // Make web variables accessible to webpack, e.g. window
};

/*
* here using hoisting so don't use `var NAME = function()...`
*/
function getImplicitGlobals() {
  return new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery',
    PUBLIC_URL: publicDir,
  });
}

function setNodeEnv() {
  return new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      PUBLIC_URL: JSON.stringify(process.env.PUBLIC_URL),
      NODE_PATH: JSON.stringify(process.env.NODE_PATH)
    },
  });
}

module.exports = config;
