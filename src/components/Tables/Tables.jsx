import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { makeStyles } from '@material-ui/core/styles';
import * as functions from 'Utils/commonFunctions';

const columns = [
  { id: 'name', label: 'Name', minWidth: 170 },
  { id: 'code', label: 'ISO\u00a0Code', minWidth: 100 },
  {
    id: 'population',
    label: 'Population',
    minWidth: 170,
    align: 'right',
    format: value => value.toLocaleString(),
  },
  {
    id: 'size',
    label: 'Size\u00a0(km\u00b2)',
    minWidth: 170,
    align: 'right',
    format: value => value.toLocaleString(),
  },
  {
    id: 'density',
    label: 'Density',
    minWidth: 170,
    align: 'right',
    format: value => value.toFixed(2),
  },
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

class Tables extends React.Component {

    renderTableCell = (row, isFromHeaders = false) => {
      const { isTableRowCellHasImage } = this.props;
      if(row && Object.keys(row).length > 0){
        return Object.keys(row).map((cell, keyIndx) => {
          if(keyIndx === 0 && isTableRowCellHasImage && !isFromHeaders){
            return (
              <TableCell key={keyIndx} component="td" scope="row" align="right">
                <img src={row[0]} />
              </TableCell>
            );
          } else {
            return (
              <TableCell key={keyIndx} component="td" scope="row" align="right">
                {row[keyIndx]}
              </TableCell>
            );
          }
        });
      }
      return;
    };

    renderTableRow = (rows, isFromHeaders = false) => {
      if(rows && rows.length > 0){
        return rows.map((row, keyIndex) => {
          return (
            <TableRow key={`${keyIndex}`}>
              {row && this.renderTableCell(row, isFromHeaders)}
            </TableRow>
          );
        })
      }
      return;
    }

    renderTableHeader = (headers) => {
      if(headers && headers.length > 0){
        return this.renderTableRow(headers, true);
      }
      return;
    }

    render(){
        const { children, value, index, tableRows, tableHeaders, tableClasses} = this.props;
        return (
            <Table className={tableClasses} aria-label="simple table">
            <TableHead>
              {tableHeaders && this.renderTableHeader(tableHeaders)}
            </TableHead>
            <TableBody>
              {tableRows && this.renderTableRow(tableRows)}
            </TableBody>
          </Table>
        );

    }
}

const exportTable = Tables;
export { exportTable as Tables };