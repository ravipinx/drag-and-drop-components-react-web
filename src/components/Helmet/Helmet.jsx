import React, {Component} from 'react';
import { Helmet as ReactHelmet } from 'react-helmet';

class Helmet extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { title, metaName, metaContent } = this.props;
    return (
        <ReactHelmet>
            <title>{title}</title>
            <meta
                name={metaName}
                content={metaContent}
            />
        </ReactHelmet>
    );
  }
}


const connectedHelmet = Helmet;
export { connectedHelmet as Helmet };
