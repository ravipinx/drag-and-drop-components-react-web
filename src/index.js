// Needed for redux-saga es6 generator support
import '@babel/polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';

const MOUNT_NODE = document.getElementById('root');
ReactDOM.render(<App />, MOUNT_NODE);

if (module.hot) {
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept([require('./app')], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render();
  });
}