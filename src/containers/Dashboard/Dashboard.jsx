import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { Helmet } from 'Components/Helmet';

import { Routes } from 'Routes';

import './Dashboard.scss';


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }

    }

    render() {
        return (
            <>
            <Helmet title="Dashboard" metaName="description" metaContent="Dashboard" />
                <div className="content-wrapper">
                    Dashboard View
                </div>
            </>
        );
    }
  
}

const connectedDashboardPage = Dashboard;
export { connectedDashboardPage as Dashboard };
