import React, { Component } from 'react';
import LogoSVG from 'Assets/images/logo.png';

class About extends Component {
    render(){
	console.log('this.props', this.props);
      return (
        <div className="App">
			<header className="App-header">
				<p>
				Edit <code>src/app/App.js</code> and save to reload.
				</p>
				<a
				className="App-link"
				href="https://reactjs.org"
				target="_blank"
				rel="noopener noreferrer"
				>
				Learn React
				</a>
			</header>
        </div>
    	);
  	}
}

const connectedAboutPage = About;
export { connectedAboutPage as About };
