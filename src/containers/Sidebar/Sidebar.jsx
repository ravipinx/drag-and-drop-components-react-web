import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './sidebar.scss';
import UserImg from 'Assets/images/me.svg'
import DailyImg from 'Assets/images/dailyview.svg'
import AppointmentImg from 'Assets/images/calendar.svg'
import PickupsImg from 'Assets/images/pickups.svg'
import AttendanceImg from 'Assets/images/attendance.svg'
import PatientsImg from 'Assets/images/patient.svg'
import FacilityImg from 'Assets/images/facility.svg'
import ContactImg from 'Assets/images/contact-us.svg'
import AboutImg from 'Assets/images/about.svg'
import SettingImg from 'Assets/images/settings.svg'
import LogOutImg from 'Assets/images/log-out.svg';
import MessageImg from 'Assets/images/msg_sidebar.svg';
import AuthorizationSidebar from 'Assets/images/authorize_sidebar.svg';
import BellImg from 'Assets/images/bell_sidebar.svg';
import ApproveImg from 'Assets/images/approve.svg';
import AdminImg from 'Assets/images/admin.svg';


import { Routes } from 'Routes';

class Sidebar extends Component {
	render() {
		return (
			<div className="app">
				<div className="sidebar-sec">
					<div className="logo">
						<div className="site-title logo-lg">
							<Link to={Routes.DASHBOARD_VIEW} title="fluiditymanager">fluidity<span>manager</span></Link>
						</div>
						<div className="site-title logo-sm">
							<Link to={Routes.DASHBOARD_VIEW} title="fluiditymanager">f<span>m</span></Link>
						</div>
					</div>
					<div className="sidebar-menu">
						<ul>
							<li className="active">
								<Link to={Routes.DASHBOARD_VIEW} title="Dashboard" className="nav-list">
									<span className="icon"><img src={UserImg} /></span><span className="nav-list"> Dashboard</span>
								</Link>
							</li>
							<li>
								<Link to={Routes.DAILYVIEW_VIEW} title="Daily view" className="nav-list">
									<span className="icon"><img src={DailyImg} /></span><span className="nav-list"> Daily View</span>
								</Link>
							</li>
							<li>
								<Link to={Routes.PATIENTS_LIST_VIEW} title="Patients" className="nav-list">
									<span className="icon"><img src={PatientsImg} /></span> <span className="nav-list"> Patients</span>
								</Link>
							</li>
							<li>
								<Link to={Routes.FACILITIES_VIEW} title="Facilities" className="nav-list">
									<span className="icon"><img src={FacilityImg} /></span> <span className="nav-list"> Facilities</span>
								</Link>
							</li>
							<li>
								<Link to={Routes.MESSAGE_VIEW} title="Message" className="nav-list">
									<span className="icon"><img src={MessageImg} /></span> <span className="nav-list"> Messages</span>
								</Link>
							</li>
							<li>
								<Link href="javascript:void(0);" title="Admin" className="nav-list">
									<span className="icon"><img src={AdminImg} /></span><span className="nav-list"> Admin</span><span className="caret"><i className="fa fa-caret-right"></i></span>
								</Link>
								<ul className="submenu dropdown">
									<li>
										<Link to={Routes.NOTIFICATIONS_VIEW} title="Notifications" className="nav-list">
											<span className="nav-list">Notifications</span>
										</Link>
									</li>
									<li>
										<Link to={Routes.AUTHORIZATIONPATIENTLIST_VIEW} title="Authorization" className="nav-list">
											<span className="nav-list"> Authorization</span>
										</Link>
									</li>
									<li>
										<Link to={Routes.APPROVALS_VIEW} title="Approvals" className="nav-list">
											<span className="nav-list">Approvals</span>
										</Link>
									</li>
									<li>
										<Link to={Routes.SETTINGS} title="Settings" className="nav-list">
											<span className="nav-list"> Settings</span>
										</Link>
									</li>
									<li>
										<Link href="javascript:void(0);" title="About" className="nav-list">
											<span className="nav-list"> About</span>
										</Link>
									</li>
									<li>
										<Link href="javascript:void(0);" title="Contact Us" className="nav-list">
											<span className="nav-list"> Contact Us</span>
										</Link>
									</li>
								</ul>
							</li>
							{/* <li >
                              <Link href="javascript:void(0);" title="Appointments" className="nav-list">
                                  <span className="icon"><img src={AppointmentImg} /></span> <span className="nav-list"> Appointments</span>
							</Link>
						</li>
						<li>
                              <Link href="javascript:void(0);" title="Pickups" className="nav-list">
                                  <span className="icon"><img src={PickupsImg} /></span> <span className="nav-list">Pickups</span>
							</Link>
						</li>
						<li>
                              <Link href="javascript:void(0);" title="Attendance" className="nav-list">
                                  <span className="icon"><img src={AttendanceImg} /></span> <span className="nav-list"> Attendance</span>
							</Link>
						</li>
						 */}

							<li>
								<Link to={Routes.LOGIN_VIEW} title="Log out" className="nav-list" onClick={() => { localStorage.clear(); }}>
									<span className="icon"><img src={LogOutImg} /></span> <span className="nav-list"> Log out</span>
								</Link>
							</li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

const connectedSidebarPage = Sidebar;
export { connectedSidebarPage as Sidebar };
