import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import { Routes } from 'Routes';
import './header.scss';
import SearchSVG from 'Assets/images/search.svg';
import MessageSVG from 'Assets/images/msg.svg';
import BellSVG from 'Assets/images/bell.svg';
import UserImg from 'Assets/images/user-3.png';
import MenuImg from 'Assets/images/menu.svg';
import CloseImg from 'Assets/images/close.png';
import LeftImg from 'Assets/images/left.svg';

class Header extends Component {

	constructor(props){
		super(props);
		this.state = {
			isNotificationOpen: false
		}
	}

	handleOnClickNotification = () => {
		this.setState((prevState) => {
			return {
				isNotificationOpen: !prevState.isNotificationOpen
			}
		})
	}

	render() {
		return (
			<div className="app-header">
				<header>
					<div className="nav-bar">
						<div className="menu-icon">
							<span><img src={MenuImg} /></span>
						</div>
						<div className="searchbar">
							<input type="text" className="form-control" placeholder="Search..."></input>
							<span className=""><img src={SearchSVG} /></span>
						</div>
						<ul className="navbar-action">
							<li>
								<a href="JavaScript:void(0);" className="icon">
									<img src={MessageSVG} />
									<span className="new-noti"></span>
								</a>
							</li>
							<li>
							<li>
								<div onClick={this.handleOnClickNotification} className="icon bell-icon">
									<img src={BellSVG} />
									<span className="new-noti"></span>
								</div>
							</li>
							</li>
							<li>
								<div className="user-profile">
									<span className="user-img"><img src={UserImg} /></span>
									<span className="user-name">Walter Melon</span>
								</div>
							</li>
						</ul>
					</div>
					<div className={`notification-list notification-sec${this.state.isNotificationOpen ? ' open': ''}`} id="notifications">
						<div className="notification-head">
							<h2>Notifications</h2>
							<span className="clear-all">Clear All</span>
						</div>
						<ul>
							
							<li className="new-notification">
								<Link to={Routes.PATIENTS_INFO_VIEW} className="noti-content">
									<p>Your upcoming <span>Task</span> is due in <span>two hours</span>.</p>
									<span className="timestamp">01 Jan 2020, 10:32 PM</span>
								</Link>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
							<li className="new-notification">
								<Link to={Routes.MESSAGE_VIEW} className="noti-content">
									<p>You have a new <span>Message</span> from <span>Malcolm Function</span>.</p>
									<span className="timestamp">01 Jan 2020, 10:32 PM</span>
								</Link>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
							<li>
								<Link to={Routes.MEDICATION_VIEW} className="noti-content">
									<p>You have a <span>Reminder</span>.</p>
									<span className="timestamp">05 Jan 2020, 10:32 PM</span>
								</Link>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
							<li>
								<div className="noti-content">
									<p><span>Malcolm Function</span> has created a <span>Task</span> and assigned it to <span>Ingredia Nutrisha</span>.</p>
									<span className="timestamp">05 Jan 2020, 10:32 PM</span>
								</div>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
							<li>
								<Link to={Routes.MESSAGE_VIEW} className="noti-content">
									<p>You have a <span>Message</span> from <span>Ingredia Nutrisha</span>.</p>
									<span className="timestamp">08 Jan 2020, 10:32 PM</span>
								</Link>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
							<li>
								<Link to={Routes.MEDICATION_VIEW} className="noti-content">
									<p>You have a <span>Reminder</span>.</p>
									<span className="timestamp">09 Jan 2020, 10:32 PM</span>
								</Link>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
							<li>
								<Link to={Routes.MESSAGE_VIEW} className="noti-content">
									<p>You have a <span>Message</span> from <span>Ingredia Nutrisha</span>.</p>
									<span className="timestamp">08 Jan 2020, 10:32 PM</span>
								</Link>
								<span className="close-icon"><img src={CloseImg} /></span>
							</li>
						</ul>
						<div className="show-all">
							<Link to={Routes.NOTIFICATIONS_VIEW} className="show-all-link">Show All Notifications</Link>
							<span><img src={LeftImg} /></span>
						</div>
					</div>
				</header>
			</div>
		);
	}
}

const connectedHeaderPage = Header;
export { connectedHeaderPage as Header };
