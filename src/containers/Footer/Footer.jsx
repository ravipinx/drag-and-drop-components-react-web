import React, { Component } from 'react';
import './footer.scss';
class Footer extends Component {
    render(){
      return (
        <div className="foo-App">
          <footer>
            <p>©2019 <span>fluiditycare</span>. All Rights Reserved.</p>
          </footer>
        </div>
    	);
  	}
}

const connectedFooterPage = Footer;
export { connectedFooterPage as Footer };
