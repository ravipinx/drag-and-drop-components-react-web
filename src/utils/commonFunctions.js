export const setItemInLocalStorage = (key = 'demo', data, isJSONStringify = true) => {
  // setter
  return localStorage.setItem(key, isJSONStringify ? JSON.stringify(data) : data);
}

export const getItemInLocalStorage = (key = 'demo', isJSONParse = true) => {
  // getter
  return localStorage.getItem(isJSONParse ? JSON.parse(key) : key);
}

export const removeItemInLocalStorage = (key = 'demo') => {
  // remove
  return localStorage.removeItem(key);
}

export const cleanLocalStorage = () => {
  // clean
  return localStorage.clear();
}
