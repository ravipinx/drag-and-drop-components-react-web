import React from 'react';
import { ConnectedRouter } from 'connected-react-router';

import { Provider } from 'react-redux';
import { Root } from '../routes';
import createStore from '../store';

import "jquery";
import "font-awesome/css/font-awesome.css";
import "react-popper";

import 'Assets/css/style.scss';
import 'Assets/js/general';

import { Routes } from 'Routes'


const { history, store } = createStore();

class App extends React.Component {
  
  constructor(){
    super();
    this.state = {};
  }

  componentDidMount() {
    if (!localStorage.getItem('hauth_token')) {
        localStorage.clear();
        history.push(Routes.LOGIN_VIEW);
    }
  }
  
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Root />
        </ConnectedRouter>
      </Provider>
    );
  }
}

const connectedApp = App;
export { connectedApp as App };