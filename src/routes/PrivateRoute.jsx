/* eslint-disable no-unused-vars */
import React from 'react';
import { Route, Redirect } from 'react-router';
import { Routes } from './Routes';

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('hauth_token')
            ? <Component {...props} />
            : <Redirect to={{ pathname: Routes.LOGIN_VIEW, state: { from: props.location } }} />
    )} />
);

export const UnprivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('hauth_token')
            ? <Redirect to={{ pathname: Routes.DASHBOARD_VIEW, state: { from: props.location } }} />
            : <Component {...props} />
    )} />
);
