import React from 'react'

class Routes extends React.Component {

    static HOME_VIEW = '/';
    static ABOUT_VIEW = '/about-us';
    static LOGIN_VIEW = '/login';

    static DASHBOARD_VIEW = '/dashboard';
}

const connectedRoutes = Routes;
export { connectedRoutes as Routes };