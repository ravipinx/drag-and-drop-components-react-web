import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Route, Switch, Redirect } from 'react-router';
// import { Home } from 'Containers/Home';
// import { About } from 'Containers/About';
import { Login } from 'Containers/Login';
import { Header } from 'Containers/Header';
import { Sidebar } from 'Containers/Sidebar';
import { Dashboard } from 'Containers/Dashboard';

import { Routes } from './Routes';
import { PrivateRoute, UnprivateRoute } from './PrivateRoute';
import $ from "jquery";

class Root extends Component {

  constructor(props){
    super(props);
    this.myDivToFocus = React.createRef();
  }
  

  componentWillReceiveProps() {
    if(this.myDivToFocus.current){
      this.myDivToFocus.current.scrollIntoView({ 
         behavior: "smooth", 
         block: "nearest"
      });
    }
  }

  render() {
    const { router } = this.props;
    const headerNotForRoutes = [Routes.LOGIN_VIEW];
    return (
      <>
        {router.location && 'pathname' in router.location && !headerNotForRoutes.includes(router.location.pathname) ? <Header /> : null}
        {router.location && 'pathname' in router.location && !headerNotForRoutes.includes(router.location.pathname) ? <Sidebar /> : null}
        <Switch>
            <Redirect exact path="/" to={Routes.DASHBOARD_VIEW} />
            <UnprivateRoute exact path={Routes.LOGIN_VIEW} component={Login} />
            <div className="contentWrapper">    
                <div ref={this.myDivToFocus}></div>
               <PrivateRoute exact path={Routes.DASHBOARD_VIEW} component={Dashboard} />  
            </div>
        </Switch>
      </>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        router: state.router
    }
}
const connectedRoot = connect(mapStateToProps)(Root);
export { connectedRoot as Root }
